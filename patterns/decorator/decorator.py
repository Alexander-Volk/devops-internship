class Component:
    """
    The base interface defines the behavior that changes
    decorators.
    """

    def operation(self):
        pass


class ConcreteComponent(Component):
    """
    Provides a default implementation of the behavior.
    """
    def operation(self):
        return "---------"


class Decorator(Component):
    """
    Defines the wrapper interface for all specific decorators.
    """
    _component: Component = None

    def __init__(self, component: Component):
        self._component = component

    @property
    def component(self):
        """
        The decorator delegates all work to the wrapped component.
        """
        return self._component

    def operation(self):
        return self._component.operation()


class ConcreteDecoratorA(Decorator):

    def operation(self):
        return f"---------\n{self.component.operation()}\n---------"


class ConcreteDecoratorB(Decorator):

    def operation(self):
        return f"\n/‾‾‾‾‾‾‾\\\n{self.component.operation()}\n\\_______/"


def client_code(component: Component):
    print(component.operation())


if __name__ == "__main__":
    simple = ConcreteComponent()
    decorator1 = ConcreteDecoratorA(simple)
    decorator2 = ConcreteDecoratorB(decorator1)
    client_code(decorator2)
