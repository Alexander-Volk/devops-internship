import time
from functools import wraps


def runtime(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.monotonic()
        func(*args, **kwargs)
        print(f'Runtime: {time.monotonic() - start}')

    return wrapper
