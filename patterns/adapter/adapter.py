from dicttoxml import dicttoxml
from covid_19 import main as dict_data


class Target:
    """
    Interface that client code can work with.
    """
    def bytes_string(self):
        return b'Target: xml format as bytes'


class Adaptee:
    """
    The interface is incompatible with existing client code.
    """
    def get_dict_data(self):
        return dict_data()


class Adapter(Target, Adaptee):
    """
    The adapter makes the interface of the adaptable class
    compatible with the target interface through multiple inheritance.
    """
    def request(self):
        xml = dicttoxml(self.get_dict_data())
        return xml


def write_xml(target: Target):
    """
    The client code supports all classes that use the target interface.
    """
    xml = target.request()
    with open('some_file1.xml', 'wb') as file:
        file.write(xml)


if __name__ == '__main__':

    adaptee = Adaptee()
    adapter = Adapter()
    write_xml(adapter)
