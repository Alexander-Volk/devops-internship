import requests
import logging.config
from random import choice
from settings import logger_config

logging.config.dictConfig(logger_config)
logger = logging.getLogger('script_logger')


def get_json(url):
    response = requests.get(url)
    logger.debug(f'link: {url} - API access code {response.status_code}')
    return response.json()


def get_country_stats(dict_data):
    random_country = choice(dict_data['Countries'])
    logger.info(f'Full information about the country: {random_country}')
    return {
        'Country': random_country.get('Country'),
        'Total Cases': random_country.get('TotalConfirmed'),
        'Total Recovered': random_country.get('TotalRecovered')
    }


def main():
    url = 'https://api.covid19api.com/summary'
    return get_country_stats(get_json(url))

