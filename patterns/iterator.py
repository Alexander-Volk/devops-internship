from random import randint


class Iterator:
    def __init__(self, limit):
        self.limit = limit
        self.counter = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.limit > 0:
            self.counter = randint(1, 10)
            self.limit -= 1
            return self.counter
        else:
            raise StopIteration


class ConcreteIterator(Iterator):
    def __init__(self, collection):
        super().__init__(collection)
        self.collection = list(collection)
        self.counter = -1

    def __next__(self):
        if self.counter + 1 < len(self.collection):
            self.counter += 1
            return self.collection[self.counter]
        raise StopIteration


if __name__ == '__main__':
    some_collection = [1, 'a', 'sdf.', (123, 34), [5, 'sdfg', [1, 3]], {'a': 1}]
    itr = ConcreteIterator(some_collection)
    print(next(itr))
    print(next(itr))
    for elem in itr:
        print(elem, end=',\t')
