class Bank:
    def __init__(self, amount_money, interest_rate_strategy=None):
        self.money = amount_money
        self.strategy = interest_rate_strategy

    def money_after_settlement(self):
        if self.strategy:
            interest_rate = self.strategy(self)
        else:
            interest_rate = 0

        return (self.money / 100 * interest_rate) + self.money

    def __repr__(self):
        return f'Available amount of money: {self.money}\n' \
               f'Have to pay: {self.money_after_settlement()}'


def officer_credit(order):
    order.interest_rate = 13.33
    return order.interest_rate


def your_money_credit(order):
    order.interest_rate = 23
    return order.interest_rate


def renaissance_credit(order):
    order.interest_rate = 32.99
    return order.interest_rate


if __name__ == "__main__":
    print(Bank(100))
    print(Bank(5000, interest_rate_strategy=officer_credit))
    print(Bank(10000, interest_rate_strategy=your_money_credit))
    print(Bank(1234, interest_rate_strategy=renaissance_credit))
