import requests
import json
from abc import ABC, abstractmethod
from random import choice


class Handler(ABC):
    """
    Declaration of methods for building a chain of handlers and data processing
    """

    @abstractmethod
    def set_next(self, handler):
        pass

    @abstractmethod
    def handle(self, data, file_name):
        pass


class AbstractHandler(Handler):
    _next_handler: Handler = None

    def set_next(self, handler):
        self._next_handler = handler
        return handler  # for simple handler communication handler_1.set_next(handler_2)

    @abstractmethod
    def handle(self, data, file_name):
        if self._next_handler:
            return self._next_handler.handle(data, file_name)
        return


class HtmlHandler(AbstractHandler):
    """
    If the response data is html - write to .html file
    """
    def handle(self, data, file_name):
        if data.encoding == 'ISO-8859-1':
            with open(f'{file_name}.html', 'w') as file:
                file.write(data.text)
            return 'OK1'
        else:
            return super().handle(data, file_name)


class JsonHandler(AbstractHandler):
    """
    If the response data is json - write to .json file
    """
    def handle(self, data, file_name):
        if data.encoding == 'UTF-8':
            with open(f'{file_name}.json', 'w') as file:
                json.dump(data.json(), file)
            return 'OK2'
        else:
            return super().handle(data, file_name)


def get_response(url):
    response = requests.get(url)
    return response


def client_code(handler: Handler):
    url = ['https://www.google.com/', 'https://api.covid19api.com/summary']
    random_url = choice(url)
    response = get_response(random_url)
    result = handler.handle(response, 'some_file')
    print(result)


if __name__ == '__main__':
    html_format = HtmlHandler()
    json_format = JsonHandler()

    html_format.set_next(json_format)

    client_code(html_format)
