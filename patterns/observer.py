from abc import ABC, abstractmethod


class Publisher(ABC):
    """
    The publisher interface declares a set of methods for managing subscribers.
    """

    @abstractmethod
    def attach(self, observer):
        """
        Attach a watcher to the publisher.
        """
        pass

    @abstractmethod
    def detach(self, observer):
        """
        Detach the observer from the publisher.
        """
        pass

    @abstractmethod
    def notify(self):
        """
        Notify all observers of the event.
        """
        pass


class TemperaturePublisher(Publisher):
    _temperature = None  # Publisher status
    _observers = []  # Subscribers list

    def attach(self, observer):
        print("Publisher: Attached an observer.")
        self._observers.append(observer)

    def detach(self, observer):
        self._observers.remove(observer)

    def notify(self):
        print("Publisher: Notifying observers...")
        for observer in self._observers:
            observer.update(self)

    def change_temperature(self, thermometer_reading):
        self._temperature = thermometer_reading
        self.notify()

    @property
    def temperature(self):
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        self._temperature = value


class Observer(ABC):
    """
    declare a notification method that publishers use to notify their subscribers.
    """

    @abstractmethod
    def update(self, publisher):
        """
        Get an update from publisher.
        """
        pass


class AbsoluteZeroObserver(Observer):
    def update(self, current_temperature):
        if current_temperature.temperature < -273.15:
            current_temperature.temperature = -273.15
            print(f'Absolute zero: {current_temperature.temperature}, wow!!')


class ColdTemperatureObserver(Observer):
    def update(self, current_temperature):
        if current_temperature.temperature < -5:
            print(f'Very cold: {current_temperature.temperature} ℃')


class HotTemperatureObserver(Observer):
    def update(self, current_temperature):
        if current_temperature.temperature > 25:
            print(f'Very hot: {current_temperature.temperature} ℃')


class NormalTemperatureObserver(Observer):
    def update(self, current_temperature):
        if current_temperature.temperature in range(-5, 26):
            print(f'Normal {current_temperature.temperature} ℃')


if __name__ == "__main__":
    publisher = TemperaturePublisher()

    observer_a = AbsoluteZeroObserver()
    publisher.attach(observer_a)

    observer_b = ColdTemperatureObserver()
    publisher.attach(observer_b)

    observer_c = HotTemperatureObserver()
    publisher.attach(observer_c)

    observer_d = NormalTemperatureObserver()
    publisher.attach(observer_d)

    publisher.change_temperature(-300)
    publisher.change_temperature(-10)
    publisher.change_temperature(30)
    publisher.change_temperature(18)
