from abc import ABC, abstractmethod


class PlanetBuilder(ABC):
    """
    The builder interface declares methods for various parts of the product objects.
    """

    @abstractmethod
    def planet(self):
        pass

    @abstractmethod
    def core(self):
        pass

    @abstractmethod
    def surface(self):
        pass

    @abstractmethod
    def atmosphere(self):
        pass

    @abstractmethod
    def magnetic_field(self):
        pass


class ConcreteBuilderPlanet(PlanetBuilder):
    """
    The Concrete Builder class implements the Builder interface and
    provides concrete implementations of the build steps.
    """

    def __init__(self):
        """
        The new builder instance must contain an empty product object.
        """
        self.reset()

    def reset(self):
        self._planet = PlanetX()

    @property
    def planet(self):
        """
        Concrete Builder provides his own methods for getting results.
        """
        planet = self._planet
        self.reset()
        return planet

    def core(self):
        self._planet.add('metal')

    def surface(self):
        self._planet.add('hard')

    def atmosphere(self):
        self._planet.add('unknown')

    def magnetic_field(self):
        self._planet.add('2 Gs')


class PlanetX:
    def __init__(self):
        self.parts = []

    def add(self, part):
        self.parts.append(part)

    def get_parts(self):
        print(f'Planet structure: {", ".join(self.parts)}')


class Director:
    """
    Responsible for performing the build steps in the established sequence.
    """

    def __init__(self):
        self._builder = None

    @property
    def builder(self):
        return self._builder

    @builder.setter
    def builder(self, builder: PlanetBuilder):
        self._builder = builder

    def first_stage_formation(self):
        self.builder.core()

    def last_stage_formation(self):
        self.builder.core()
        self.builder.surface()
        self.builder.magnetic_field()


if __name__ == '__main__':
    director = Director()
    builder = ConcreteBuilderPlanet()
    director.builder = builder

    director.last_stage_formation()
    builder.planet.get_parts()

