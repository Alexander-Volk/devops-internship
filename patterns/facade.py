import cpuinfo
import psutil
import shutil


class CPU:
    def __init__(self):
        self.full_info = cpuinfo.cpuinfo.get_cpu_info()

    def architecture(self):
        return self.full_info['arch']

    def count_core(self):
        return self.full_info['count']

    def brand_raw(self):
        return self.full_info['brand_raw']

    def cpu_frequency(self):
        return self.full_info['hz_advertised_friendly']


class Memory:
    def __init__(self):
        self.ram = psutil.virtual_memory()

    def total(self):
        return self.ram[0] / 1024 ** 2

    def available(self):
        return self.ram[1] / 1024 ** 2

    def used(self):
        return self.ram[3] / 1024 ** 2


class HardDrive:
    def __init__(self):
        self.hard_drive_info = shutil.disk_usage('/')

    def total(self):
        return self.hard_drive_info[0] / 1024 ** 3

    def used(self):
        return self.hard_drive_info[1] / 1024 ** 3

    def free(self):
        return self.hard_drive_info[2] / 1024 ** 3


class Computer(object):
    def __init__(self):
        self._cpu = CPU()
        self._memory = Memory()
        self._hard_drive = HardDrive()

    def info_computer(self):
        return f'CPU:\n{self._cpu.cpu_frequency()}, ' \
               f'{self._cpu.architecture()}, ' \
               f'{self._cpu.brand_raw()}, ' \
               f'{self._cpu.count_core()}\n ' \
               f'Memory:\n{self._memory.total()}, ' \
               f'{self._memory.used()}, ' \
               f'{self._memory.available()}\n ' \
               f'Hard drive:\n{self._hard_drive.total()}, ' \
               f'{self._hard_drive.used()}, ' \
               f'{self._hard_drive.free()}, '


if __name__ == '__main__':
    facade = Computer()
    print(facade.info_computer())
