from abc import ABC, abstractmethod


class SpaceObjectCreator(ABC):
    """
    Factory method that returns the space object as a product.
    """

    @abstractmethod
    def factory_method(self):
        pass

    def object_label(self):
        product = self.factory_method()

        result = f'Space object {product.name()} ' \
                 f'from the group {product.group()} ' \
                 f'formed in {product.site_formation()}'
        return result


class ConcreteCreatorPlanet(SpaceObjectCreator):
    def factory_method(self):
        return PlanetX()


class ConcreteCreatorStar(SpaceObjectCreator):
    def factory_method(self):
        return SomeStar()


class SpaceObjectProduct(ABC):
    """
    Product interface declares the methods that each product must have.
    """

    @abstractmethod
    def site_formation(self):
        pass

    @abstractmethod
    def group(self):
        pass

    @abstractmethod
    def name(self):
        pass


class PlanetX(SpaceObjectProduct):
    """
    The specific product implements the "product" interface.
    """
    def site_formation(self):
        return 'far far away'

    def group(self):
        return 'terrestrial'

    def name(self):
        return 'Kepler-186-f'


class SomeStar(SpaceObjectProduct):
    """
    The specific product implements the "product" interface.
    """

    def site_formation(self):
        return 'far far away'

    def group(self):
        return 'O'

    def name(self):
        return 'Kepler-186'


def get_info(creator: SpaceObjectCreator):
    """
    The client function works with an instance of a specific creator.
    """
    print(f'{creator.object_label()}')


if __name__ == '__main__':
    get_info((ConcreteCreatorPlanet()))
    get_info((ConcreteCreatorStar()))
