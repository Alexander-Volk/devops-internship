#!/bin/bash

while read -r country; do
  filtered_country=$(echo "$country" | grep -vi "a")
  if [[ "$filtered_country" != "" ]]; then
    country_names+=("$filtered_country")
  fi
done

echo "${country_names[@]}"
