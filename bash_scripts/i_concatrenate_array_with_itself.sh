#!/bin/bash

while read -r country || [ -n "$country" ]; do
  country_names+=("$country")
done

countries_concat_result=("${country_names[@]}" "${country_names[@]}" "${country_names[@]}")
echo "${countries_concat_result[@]}"
