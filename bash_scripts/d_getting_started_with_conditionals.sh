#!/bin/bash

read -r symbol
if [ "$symbol" = "y" ] || [ "$symbol" = "Y" ]; then
  echo YES
elif [ "$symbol" = "n" ] || [ "$symbol" = "N" ]; then
  echo NO
fi
