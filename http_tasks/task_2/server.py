import socket
import os
from http_tasks.task_2.create_images import create_jpg_image

SERVER_ADDRESS = ('localhost', 9000)
MAX_CONNECT = 10


def create_socket(server_address, max_connect):
    some_socket = socket.socket(
        socket.AF_INET,
        socket.SOCK_STREAM,
    )
    some_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    some_socket.bind(server_address)
    some_socket.listen(MAX_CONNECT)
    return some_socket


def create_env():
    if 'IMAGE_PATH' not in os.environ:
        print('У вас не создана переменная окружения IMAGE_PATH="где сохранить изображение"')
        os.environ['IMAGE_PATH'] = input('Где сохрнить изображение, полный путь: ')
        print(os.getenv('IMAGE_PATH'))


def main():
    print('Server is running')
    server = create_socket(SERVER_ADDRESS, MAX_CONNECT)
    with server:
        connection, address = server.accept()
        print(f'New connection from {address}')
        myfile = open('image.txt', 'wb')
        while True:
            data = connection.recv(1024)
            if not data:
                break
            myfile.write(data)
            # connection.close()
        myfile.close()


if __name__ == '__main__':
    create_env()
    main()
    create_jpg_image('image.txt')
