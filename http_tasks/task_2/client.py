import requests
import os


def path_to_image():
    while True:
        image_file = os.path.abspath(input('Введите путь к изображению: '))
        print(image_file)
        if os.path.exists(image_file):
            return image_file
        print('Неверный путь к изображению')


def send_picture(url):
    with open(path_to_image(), 'rb') as file:
        files = {'file': file}
        headers = {'Content-type': 'image/jpg'}
        try:
            requests.post(url, files=files, headers=headers)
        except requests.exceptions.ConnectionError:
            print('Сервер недоступен')
        except requests.exceptions.InvalidURL:
            print('Неккоректный ip или порт')


if __name__ == '__main__':
    url = f'http://{input("Введите ip сервера: ")}:{input("Введите порт сервера: ")}'
    send_picture(url)
