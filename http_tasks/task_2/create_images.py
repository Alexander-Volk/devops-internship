# Ужасный костыль
import os


def create_jpg_image(path_to_file):
    with open(path_to_file, 'rb') as text_file:
        one_list = []
        two_list = []
        for line in text_file.readlines():
            if line.endswith(b'\r\n'):
                one_list.append(line)
            else:
                two_list.append(line)

    with open(f"{os.getenv('IMAGE_PATH')}/{input('Как назвать изображение: ')}.jpg", 'wb') as image:
        last_str = one_list[-2].rstrip()
        for line in two_list:
            image.write(line)
        image.write(last_str)
