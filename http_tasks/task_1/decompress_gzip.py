import requests
import zlib

# 16 + (8 to 15) Uses the low 4 bits of the value as the window size logarithm.
# The input must include a gzip header and trailer.
BUFFER_SIZE = 16 + zlib.MAX_WBITS


def decompress_stream(stream):
    decompression_object = zlib.decompressobj(BUFFER_SIZE)

    for chunk in stream:
        yield decompression_object.decompress(chunk)
    yield decompression_object.flush()


def get_gzip(url):
    headers = {'accept-encoding': 'gzip'}
    response = requests.get(url, stream=True, headers=headers)
    return response


def write_decompressed_content(stream):
    path_file = input('Where to write the file: ')
    with open(path_file, 'wb') as file:
        for chunk in decompress_stream(stream.raw.stream()):
            if chunk:
                file.write(chunk)
        file.flush()


if __name__ == '__main__':
    url = 'https://api.covid19api.com/summary'
    write_decompressed_content(get_gzip(url))
