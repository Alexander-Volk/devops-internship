import socket

SERVER_ADDRESS = ('localhost', 9000)
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(SERVER_ADDRESS)


with client:
    while True:
        client.send('Client massage '.encode('utf-8'))
        data = client.recv(1024)
        print(data.decode('utf-8'))
