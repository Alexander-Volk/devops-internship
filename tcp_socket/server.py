import socket
import time


SERVER_ADDRESS = ('localhost', 9000)
MAX_CONNECT = 10


def create_socket(server_address, max_connect):
    some_socket = socket.socket(
        socket.AF_INET,
        socket.SOCK_STREAM,
        # socket.SO_LINGER,
    )
    some_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    some_socket.bind(server_address)
    some_socket.listen(max_connect)
    return some_socket


def main():
    print('Server is running')
    server = create_socket(SERVER_ADDRESS, MAX_CONNECT)
    with server:
        connection, address = server.accept()
        print(f'New connection from {address}')
        connection.send('Server message'.encode('utf-8'))
        while True:
            time.sleep(1)
            data = connection.recv(1024)
            print(data.decode('utf-8'))
            # if not data:
            #     break


main()
